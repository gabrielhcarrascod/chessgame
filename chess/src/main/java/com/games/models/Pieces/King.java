package com.games.models.Pieces;
public class King extends Piece {

    private String symbol = "K";

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
}
