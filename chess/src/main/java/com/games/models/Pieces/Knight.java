package com.games.models.Pieces;
public class Knight extends Piece {

    private String symbol = "N";

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
}
