package com.games.models.Pieces;
public class Rook extends Piece {

    private String symbol = "R";

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
}
