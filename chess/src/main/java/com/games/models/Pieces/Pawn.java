package com.games.models.Pieces;
public class Pawn extends Piece {
    
    private String symbol;// = 'p';
    private int convertToPiece;
    private Boolean isFirstMove;
    private int value;

    public Pawn() {

        symbol = "p";
        isFirstMove = true;
        value = 1;
    }
    
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getConvertToPiece() {
        return convertToPiece;
    }

    public void setConvertoToPiece(int convertToPiece) {
        this.convertToPiece = convertToPiece;
    }

    public Boolean getIsFirstMove() {
        return isFirstMove;
    }

    public void setIsFirstMove(boolean isFirstMove) {
        this.isFirstMove = isFirstMove;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
