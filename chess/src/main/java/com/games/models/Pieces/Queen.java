package com.games.models.Pieces;
public class Queen extends Piece {

    private String symbol = "Q";

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
}
