package com.games.models.Pieces;
public class Bishop extends Piece {

    private String symbol = "B";

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
}
