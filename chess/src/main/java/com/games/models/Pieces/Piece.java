package com.games.models.Pieces;

import com.games.constants.ColorPieces;

public class Piece {

    private String symbol;
    private ColorPieces colorPiece;
    private int nextPositionX;
    private int nextPositionY;
    private int currentPositionX;
    private int currentPositionY;
    private Boolean isAlive;
    private int value;

    public Piece() {
        isAlive = true;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol){
        this.symbol = symbol;
    }

    public ColorPieces getColorPiece() {
        return colorPiece;
    }

    public void setColorPiece(ColorPieces colorPiece) {
        this.colorPiece = colorPiece;
    }

    public void setPositionsXandY(int currentPositionX, int currentPositionY) {
        this.currentPositionX = currentPositionX;
        this.currentPositionY = currentPositionY;
    }

    public void setNextPositionsXandY(int nextPositionX, int nextPositionY) {
        this.nextPositionX = nextPositionX;
        this.nextPositionY = nextPositionY;
    }

    public int getCurrentPositionX() {
        return currentPositionX;
    }

    public void setCurrentPositionX(int currentPositionX) {
        this.currentPositionX = currentPositionX;
    }

    public int getCurrentPositionY() {
        return currentPositionY;
    }

    public void setCurrentPositionY(int currentPositionY) {
        this.currentPositionY = currentPositionY;
    }

    public int getNextPositionX() {
        return nextPositionX;
    }

    public void setNextPositionX(int nextPositionX) {
        this.nextPositionX = nextPositionX;
    }

    public int getNextPositionY() {
        return nextPositionY;
    }

    public void setNextPositionY(int nextPositionY) {
        this.nextPositionY = nextPositionY;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}