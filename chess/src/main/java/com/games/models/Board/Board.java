package com.games.models.Board;

import java.util.ArrayList;

import com.games.models.Pieces.Piece;

import com.games.initialization.InitialPositioning;
import com.games.piecesMovement.PieceMovementInput;

public class Board {

    private Piece[][] boardWithPieces;
    private int[] boardNumbers = new int[]{8, 7, 6, 5, 4, 3, 2, 1};
    private char[] boardLetters = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    InitialPositioning initialPositioning;
    PieceMovementInput pieceMovementInput;

    public Piece[][] getBoardWithPieces() {
        return boardWithPieces;
    }

    public void setBoardWithPieces(Piece[][] boardWithPieces) {
        this.boardWithPieces = boardWithPieces;
    }

    /*public void printBoardWithPieces() {

        boardWithPieces = new Piece[8][8];
        int indexOfMatrix;

        for(int x=0; x < boardWithPieces.length; x++) {
            System.out.print(boardNumbers[x] + " ");
            for(int y=0; y < boardWithPieces.length; y++) {
                indexOfMatrix = (x + y);
                if(indexOfMatrix%2 != 0) {
                    System.out.print(ANSI_BLACK_BACKGROUND + "[" + " " + "]" + ANSI_RESET);
                }
                else {
                    System.out.print(ANSI_WHITE_BACKGROUND + "[" + " " + "]" + ANSI_RESET);
                }
            }
            System.out.println();
        }
        System.out.print("   ");
        for(int letters = 0; letters < boardLetters.length; letters++) {
            System.out.print(boardLetters[letters] + "  ");
        }
    }*/

    public void printBoardWithPieces(ArrayList<Piece> pieces) {

        initialPositioning = new InitialPositioning();
        pieceMovementInput = new PieceMovementInput();
        boardWithPieces = new Piece[8][8];
        String space = " ";

        for(int x=0; x < boardWithPieces.length; x++) {
            System.out.print(boardNumbers[x] + " ");
            for(int y=0; y < boardWithPieces.length; y++) {
                System.out.print("[");
                if(getAlivePiecetoBoardByCoordenates(pieces, x, y) != null) {
                    System.out.print(getAlivePiecetoBoardByCoordenates(pieces, x, y)); 
                } 
                else {
                    System.out.print(space);
                }
                
                System.out.print("]");
            }
            System.out.println();
        }
        System.out.print("   ");
        for(int letters = 0; letters < boardLetters.length; letters++) {
            System.out.print(boardLetters[letters] + "  ");
        }
        System.out.println();       
    }

    public String getAlivePiecetoBoardByCoordenates(ArrayList<Piece> pieces, int x, int y) {

        String alivePieceSymbol = null;

        for(Piece piece : pieces) {
            if(piece.getCurrentPositionX() == x && piece.getCurrentPositionY() == y) {
                if(piece.getIsAlive() == true) {
                    alivePieceSymbol = piece.getSymbol();
                }

            }
        }

        return alivePieceSymbol;
    } 


}
