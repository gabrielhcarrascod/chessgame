package com.games.models.Players;

import java.util.List;

import com.games.constants.ColorPieces;

public class Player {
    
    private String playerName;
    private int currentScore;
    private ColorPieces colorPiece;
    private List<String> moveHistory; 
    private Boolean isActiveTurn;
    private Boolean isAlive;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public ColorPieces getColorPiece() {
        return colorPiece;
    }

    public void setColorPiece(ColorPieces colorPiece) {
        this.colorPiece = colorPiece;
    }

    public List<String> getMoveHistory() {
        return moveHistory;
    }

    public void setMoveHistory(String lastMove) {
        moveHistory.add(lastMove);
    }

    public Boolean getIsActiveTurn() {
        return this.isActiveTurn;
    }

    public void setIsActiveTurn(Boolean isActiveTurn) {
        this.isActiveTurn = isActiveTurn;
    }

    public Boolean getIsAlive() {
        return this.isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }
}
