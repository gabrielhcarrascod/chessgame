package com.games.initialization;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games.constants.ColorPieces;
import com.games.gameModes.PlayerVsIAMode;
import com.games.gameModes.PlayerVsPlayerMode;
import com.games.gameModes.PracticeMode;
import com.games.menu.MainMenu;
import com.games.models.Players.Player;

import lombok.extern.log4j.Log4j;

public class InitializeGame {
    
    MainMenu mainMenu;
    PlayerVsIAMode playerVsIAMode = new PlayerVsIAMode();
    PlayerVsPlayerMode playerVsPlayerMode = new PlayerVsPlayerMode();
    PracticeMode practiceMode = new PracticeMode();
    Player[] players = new Player[2];
    int firstSelectedColor;

    static Logger logger = LoggerFactory.getLogger(Log4j.class.getName());

    public void createPlayerByNameAndColor(String playerName, int playerColor) { 

        Player blackPlayer = new Player();
        Player whitePlayer = new Player();

        blackPlayer.setColorPiece(ColorPieces.BLACK);
        whitePlayer.setColorPiece(ColorPieces.WHITE);

        

        switch (playerColor) {
            case 1:
                whitePlayer.setPlayerName(playerName);
                whitePlayer.setIsAlive(true);
                whitePlayer.setIsActiveTurn(true);
                break;
            case 2:
                blackPlayer.setPlayerName(playerName);
                blackPlayer.setIsAlive(true);
                blackPlayer.setIsActiveTurn(false);
                break;
        
            default:
                logger.error(" {} is not a color. Please choose 1 or 2!", playerColor);
                break;
        }

        
    }

    public void startSelectedGameMode(int selectGameMode) {

        switch (selectGameMode) {
            case 1:
                playerVsIAMode.startGame();
                break;
            case 2:
                playerVsPlayerMode.startGame();
                break;
            case 3:
                practiceMode.startGame();
                break;
            default:
                logger.error("That is not a game mode! Choose 1, 2 or 3");
                break;
        }
    }
}
