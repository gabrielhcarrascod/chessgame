package com.games.initialization;

import java.util.ArrayList;

import com.games.models.Board.Board;
import com.games.models.Pieces.King;
import com.games.models.Pieces.Bishop;
import com.games.models.Pieces.Knight;
import com.games.models.Pieces.Pawn;
import com.games.models.Pieces.Piece;
import com.games.models.Pieces.Queen;
import com.games.models.Pieces.Rook;

public class InitialPositioning {

    ArrayList<Piece> listOfPiecesInGame = new ArrayList<Piece>();

    Board board = new Board();
    
    Pawn whitePeon1 = new Pawn();
    Pawn whitePeon2 = new Pawn();
    Pawn whitePeon3 = new Pawn();
    Pawn whitePeon4 = new Pawn();
    Pawn whitePeon5 = new Pawn();
    Pawn whitePeon6 = new Pawn();
    Pawn whitePeon7 = new Pawn();
    Pawn whitePeon8 = new Pawn();

    Rook whiteTorre1 = new Rook();
    Rook whiteTorre2 = new Rook();

    Knight whiteCaballo1 = new Knight();
    Knight whiteCaballo2 = new Knight();

    Bishop whiteAlfil1 = new Bishop();
    Bishop whiteAlfil2 = new Bishop();

    Queen whiteDama = new Queen();
    King whiteRey = new King();

    Pawn blackPeon1 = new Pawn();
    Pawn blackPeon2 = new Pawn();
    Pawn blackPeon3 = new Pawn();
    Pawn blackPeon4 = new Pawn();
    Pawn blackPeon5 = new Pawn();
    Pawn blackPeon6 = new Pawn();
    Pawn blackPeon7 = new Pawn();
    Pawn blackPeon8 = new Pawn();

    Rook blackTorre1 = new Rook();
    Rook blackTorre2 = new Rook();

    Knight blackCaballo1 = new Knight();
    Knight blackCaballo2 = new Knight();

    Bishop blackAlfil1 = new Bishop();
    Bishop blackAlfil2 = new Bishop();
    Queen blackDama = new Queen();
    King blackRey = new King();

    public void addInitialPositionToPieces() {

        blackPeon1.setPositionsXandY(1, 0);
        blackPeon2.setPositionsXandY(1, 1);
        blackPeon3.setPositionsXandY(1, 2);
        blackPeon4.setPositionsXandY(1, 3);
        blackPeon5.setPositionsXandY(1, 4);
        blackPeon6.setPositionsXandY(1, 5);
        blackPeon7.setPositionsXandY(1, 6);
        blackPeon8.setPositionsXandY(1, 7);
        blackTorre1.setPositionsXandY(0, 0);
        blackTorre2.setPositionsXandY(0, 7);
        blackCaballo1.setPositionsXandY(0, 1);
        blackCaballo2.setPositionsXandY(0, 6);
        blackAlfil1.setPositionsXandY(0, 2);
        blackAlfil2.setPositionsXandY(0, 5);
        blackDama.setPositionsXandY(0, 3);
        blackRey.setPositionsXandY(0, 4);

        whitePeon1.setPositionsXandY(6, 0);
        whitePeon2.setPositionsXandY(6, 1);
        whitePeon3.setPositionsXandY(6, 2);
        whitePeon4.setPositionsXandY(6, 3);
        whitePeon5.setPositionsXandY(6, 4);
        whitePeon6.setPositionsXandY(6, 5);
        whitePeon7.setPositionsXandY(6, 6);
        whitePeon8.setPositionsXandY(6, 7);
        whiteTorre1.setPositionsXandY(7, 0);
        whiteTorre2.setPositionsXandY(7, 7);
        whiteCaballo1.setPositionsXandY(7, 1);
        whiteCaballo2.setPositionsXandY(7, 6);
        whiteAlfil1.setPositionsXandY(7, 2);
        whiteAlfil2.setPositionsXandY(7, 5);
        whiteDama.setPositionsXandY(7, 3);
        whiteRey.setPositionsXandY(7, 4);

        listOfPiecesInGame.add(whitePeon1);
        listOfPiecesInGame.add(whitePeon2);
        listOfPiecesInGame.add(whitePeon3);
        listOfPiecesInGame.add(whitePeon4);
        listOfPiecesInGame.add(whitePeon5);
        listOfPiecesInGame.add(whitePeon6);
        listOfPiecesInGame.add(whitePeon7);
        listOfPiecesInGame.add(whitePeon8);
        listOfPiecesInGame.add(whiteTorre1);
        listOfPiecesInGame.add(whiteTorre2);
        listOfPiecesInGame.add(whiteAlfil1);
        listOfPiecesInGame.add(whiteAlfil2);
        listOfPiecesInGame.add(whiteCaballo1);
        listOfPiecesInGame.add(whiteCaballo2);
        listOfPiecesInGame.add(whiteDama);
        listOfPiecesInGame.add(whiteRey);

        listOfPiecesInGame.add(blackPeon1);
        listOfPiecesInGame.add(blackPeon2);
        listOfPiecesInGame.add(blackPeon3);
        listOfPiecesInGame.add(blackPeon4);
        listOfPiecesInGame.add(blackPeon5);
        listOfPiecesInGame.add(blackPeon6);
        listOfPiecesInGame.add(blackPeon7);
        listOfPiecesInGame.add(blackPeon8);
        listOfPiecesInGame.add(blackTorre1);
        listOfPiecesInGame.add(blackTorre2);
        listOfPiecesInGame.add(blackAlfil1);
        listOfPiecesInGame.add(blackAlfil2);
        listOfPiecesInGame.add(blackCaballo1);
        listOfPiecesInGame.add(blackCaballo2);
        listOfPiecesInGame.add(blackDama);
        listOfPiecesInGame.add(blackRey);

        board.printBoardWithPieces(listOfPiecesInGame);

    }
    
}
