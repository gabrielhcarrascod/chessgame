package com.games.playerTurnManagement;

import java.util.ArrayList;

import com.games.models.Players.Player;
import com.games.piecesMovement.PieceMovementInput;

public class playerTurnManagementServiceImpl implements playerTurnManagementService {

    public Boolean switchPlayerTurnAfterMove(Player player) throws Exception {

        Boolean currentPlayerTurn = null;

        PieceMovementInput pieceMovementInput = new PieceMovementInput();

        if(player.getIsActiveTurn()) {
            try {
                pieceMovementInput.enterPlayersMove();
            }
            catch(Exception e) {
                throw new Exception("There is a problem switching players Turn", e);
            }
            player.setIsActiveTurn(false);
            currentPlayerTurn = player.getIsActiveTurn();
            
        }

        return currentPlayerTurn;

    }
    
    public void whiteStartsTheGame(ArrayList<Player> players) {
        
    }
}
