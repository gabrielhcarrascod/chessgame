package com.games.playerTurnManagement;

import java.util.ArrayList;
import java.util.List;

import com.games.models.Players.Player;

public interface playerTurnManagementService {

    public Boolean switchPlayerTurnAfterMove(Player player) throws Exception;
    public void whiteStartsTheGame(ArrayList<Player> players);
    
}
