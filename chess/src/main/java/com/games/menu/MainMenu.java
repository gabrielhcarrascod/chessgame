package com.games.menu;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.games.initialization.InitializeGame;
import lombok.extern.log4j.Log4j;
import com.games.constants.ColorPieces;

public class MainMenu {

    static Logger logger = LoggerFactory.getLogger(Log4j.class.getName());

    Scanner scanner;
    ColorPieces playerColorPieces;
    StringBuilder stringBuilder = new StringBuilder();
    InitializeGame initializeGame = new InitializeGame();
    
    public void selectGameMode() {

        scanner = new Scanner(System.in);
        logger.info("Select game mode: ");
        logger.info("\n");
        logger.info("     Player vs IA      -> 1     ");
        logger.info("     Player vs Player  -> 2     ");
        logger.info("     Practice Mode     -> 3     ");
        logger.info("--------------------------------");
        logger.info("\n");
        int gameMode = scanner.nextInt();
        initializeGame.startSelectedGameMode(gameMode);
    }

    public void enterPlayerInfo() {

        scanner = new Scanner(System.in);
        logger.info("Enter player name: ");
        logger.info("\n");
        String playerName = scanner.nextLine();
        logger.info("Player: " + playerName + " chooses color!");
        logger.info("\n");
        logger.info("     white -> 1     ");
        logger.info("     black -> 2     ");
        logger.info("--------------------------------");
        logger.info("\n");
        int playerColor = scanner.nextInt();
        initializeGame.createPlayerByNameAndColor(playerName, playerColor);
    }


}
