package com.games.piecesMovement;

public interface PiecesMovementService {

    public static void pieceMovementDirection(int newX, int newY) {}

    public static void pieceIsMovingForward() {}

    public static void pieceIsMovingBackwards() {}

    public static void pieceIsMovingToTheLeft() {}

    public static void pieceIsMovingToTheRight() {}

    public static void pieceIsMovingToRightUpwards() {}

    public static void pieceIsMovingToLeftUpwards() {}

    public static void pieceIsMovingToRightDownwards() {}

    public static void pieceIsMovingToLeftDownwards() {}
    
}
