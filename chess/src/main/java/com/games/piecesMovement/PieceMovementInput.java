package com.games.piecesMovement;

import java.util.Scanner;

public class PieceMovementInput {

    Scanner scanner;
    PiecesMovementService piecesMovementService;
    
    public void enterPlayersMove() {

        scanner = new Scanner(System.in);
        String move = null;
        
        System.out.println("Enter move: ");
        System.out.println();
        try {
            move = scanner.nextLine();
        }
        catch(Exception e) {
            System.out.println("Enter a valid move!");
        }
        
        getChessCoordinatesFromPlayerInpu(move);

    }

    public void getChessCoordinatesFromPlayerInpu(String move) {

        String coordinateX = null;
        int coordinateY = 0;

        if(move.length() == 3) {
            coordinateX = String.valueOf(move.charAt(1));
            coordinateY = Integer.parseInt(String.valueOf(move.charAt(2)));
        }
        if(move.length() == 2) {
            coordinateX = String.valueOf(move.charAt(0));
            coordinateY = Integer.parseInt(String.valueOf(move.charAt(1)));
        }
        if(move.length() > 3 || move.length() < 2) {
            System.out.println("--The move has invalied lenght!--");
        }

        parseMoveToMatrixCoordenates(coordinateX, coordinateY);

    }

    public void parseMoveToMatrixCoordenates(String txtCoordinateX, int wrongCoordinateY) {

        int coordinateX = 0;
        int coordinateY = 0;

        switch (txtCoordinateX) {
            case "a":
                coordinateX = 0;
                break;
            case "b":
                coordinateX = 1;
                break;
            case "c":
                coordinateX = 2;
                break;
            case "d":
                coordinateX = 3;
                break;
            case "e": 
                coordinateX = 4;
                break;
            case "f": 
                coordinateX = 5;
                break;
            case "g": 
                coordinateX = 6;
                break;
            case "h":
                coordinateX = 7;
                break;
            default:
                System.out.println("New coordinate: " + coordinateX);
                System.out.println("Don't know what happened in: parseMoveToMatrixCoordenatesX");
                break;
        }

        switch (wrongCoordinateY) {
            case 1:
                coordinateY = 7;
                break;
            case 2:
                coordinateY = 6;
                break;
            case 3: 
                coordinateY = 5;    
            break;
            case 4: 
                coordinateY = 4;
                break;
            case 5: 
                coordinateY = 3;
                break;
            case 6: 
                coordinateY = 2;
                break;
            case 7: 
                coordinateY = 1;
                break;
            case 8: 
                coordinateY = 0;
                break;
            default:
            System.out.println("Don't know what happened in: parseMoveToMatrixCoordenatesY");
                break;
        }
        //System.out.println("Cooordinates: " + "[" + coordinateX + ", " + coordinateY + "]");

        PiecesMovementService.pieceMovementDirection(coordinateX, coordinateY);
    }

}
