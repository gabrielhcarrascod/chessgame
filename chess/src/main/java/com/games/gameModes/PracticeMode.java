package com.games.gameModes;

import com.games.initialization.InitialPositioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PracticeMode {

    static Logger logger = LoggerFactory.getLogger(Logger.class.getName());

    InitialPositioning initialPositioning;

    public void startGame() {

        initialPositioning = new InitialPositioning();
        
        System.out.println("---Practice Mode---");
        initialPositioning.addInitialPositionToPieces();

    }
    
}
