package com.games.gameModes;

import com.games.menu.MainMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayerVsPlayerMode {

    static Logger logger = LoggerFactory.getLogger(Logger.class.getName());

    MainMenu mainMenu;
    
    public void startGame() {

        mainMenu.enterPlayerInfo();
        
    }
    
}
